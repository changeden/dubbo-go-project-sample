module dubbo-go-project-sample

go 1.16

require (
	dubbo.apache.org/dubbo-go/v3 v3.0.1
	gitee.com/changeden/dubbo-go-middleware-database v0.1.5
	gitee.com/changeden/dubbo-go-middleware-dubbo v0.1.5
	gitee.com/changeden/dubbo-go-middleware-redis v0.1.5
	gitee.com/changeden/dubbo-go-middleware-web v0.1.5
	gitee.com/changeden/dubbo-go-starter v0.1.5
	github.com/apache/dubbo-go-hessian2 v1.11.0
	github.com/gin-gonic/gin v1.7.7
)
