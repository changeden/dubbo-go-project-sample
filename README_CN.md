[English](./README_CN.md)

# dubbo-go-project-sample

## 项目目录结构
```
|- .run -------------------- Intellij运行配置
|- cmd --------------------- 程序入口
|- controller -------------- 控制器层
   |- controller_entry.go -- Web服务路由处理管理工具
|- dto --------------------- 数据传输对象层
|- resources --------------- 程序配置及静态资源
   |- application.yml ------ 程序配置文件
|- service ----------------- 服务层，包含了公共服务及Dubbo服务
```

## 启动项目

```shell
# 添加环境变量
set CONF_APPLICATION_FILE_PATH=项目地址/resources/application.yml
go run cmd/main.go
```

## 组件

| 名称                           | 包                                                                                                            | 描述              |  最新版本  |
|:-----------------------------|:-------------------------------------------------------------------------------------------------------------|:----------------|:------:|
| dubbo-go-starter             | [gitee.com/changeden/dubbo-go-starter](https://gitee.com/changeden/dubbo-go-starter)                         | dubbo-go 项目启动套件 | v0.1.5 |
| dubbo-go-middleware-database | [gitee.com/changeden/dubbo-go-middleware-database](https://gitee.com/changeden/dubbo-go-middleware-database) | 数据库中间件          | v0.1.5 |
| dubbo-go-middleware-redis    | [gitee.com/changeden/dubbo-go-middleware-redis](https://gitee.com/changeden/dubbo-go-middleware-redis)       | 内存缓存中间件         | v0.1.5 |
| dubbo-go-middleware-web      | [gitee.com/changeden/dubbo-go-middleware-web](https://gitee.com/changeden/dubbo-go-middleware-web)           | Web服务中间件        | v0.1.5 |
| dubbo-go-middleware-dubbo    | [gitee.com/changeden/dubbo-go-middleware-dubbo](https://gitee.com/changeden/dubbo-go-middleware-dubbo)       | dubbo服务中间件      | v0.1.5 |
