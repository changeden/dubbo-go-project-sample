package main

import (
	"dubbo-go-project-sample/controller"
	"dubbo.apache.org/dubbo-go/v3/common/logger"
	databaseMiddleware "gitee.com/changeden/dubbo-go-middleware-database/middleware"
	dubboMiddleware "gitee.com/changeden/dubbo-go-middleware-dubbo/middleware"
	redisMiddleware "gitee.com/changeden/dubbo-go-middleware-redis/middleware"
	webMiddleware "gitee.com/changeden/dubbo-go-middleware-web/middleware"
	"gitee.com/changeden/dubbo-go-starter"
	"github.com/gin-gonic/gin"
)

func onWebMiddlewareSetup(router *gin.Engine) {
	controller.SetRouter(router)
	logger.Info("web middleware setup")
}

func onDatabaseMiddlewareSetup() {
	logger.Info("database middleware setup")
}

func onRedisMiddlewareSetup() {
	logger.Info("redis middleware setup")
}

func onDubboMiddlewareSetup() {
	logger.Info("dubbo-go middleware setup")
}

// export CONF_APPLICATION_FILE_PATH=ProjectPath/resources/application.yml
func main() {
	err := starter.NewStarter().AddMiddlewareSetupHooks(
		webMiddleware.NewWebSetupHook(onWebMiddlewareSetup),
		databaseMiddleware.NewDatabaseSetupHook(onDatabaseMiddlewareSetup),
		redisMiddleware.NewRedisSetupHook(onRedisMiddlewareSetup),
		dubboMiddleware.NewDubboSetupHook(onDubboMiddlewareSetup),
	).Start()
	if err != nil {
		logger.Error(err)
		return
	}
}
