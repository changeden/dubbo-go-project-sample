[中文](./README_CN.md)

# dubbo-go-project-sample

## Directory Structure

```
|- .run -------------------- Intellij run configuration
|- cmd --------------------- Application entry
|- controller -------------- Router handler layer
   |- controller_entry.go -- Web service router handler manager
|- dto --------------------- Data transfer object layer
|- resources --------------- Application config file and static files
   |- application.yml ------ Application config file
|- service ----------------- Service layer, includes common services and dubbo services
```

## Start

```shell
# Add Env
set CONF_APPLICATION_FILE_PATH=ProjectPath/resources/application.yml
go run cmd/main.go
```

## Dependence

| name                         | package                                                                                                      | description                 | version |
|:-----------------------------|:-------------------------------------------------------------------------------------------------------------|:----------------------------|:-------:|
| dubbo-go-starter             | [gitee.com/changeden/dubbo-go-starter](https://gitee.com/changeden/dubbo-go-starter)                         | dubbo-go project boot suite | v0.1.5  |
| dubbo-go-middleware-database | [gitee.com/changeden/dubbo-go-middleware-database](https://gitee.com/changeden/dubbo-go-middleware-database) | database middleware         | v0.1.5  |
| dubbo-go-middleware-redis    | [gitee.com/changeden/dubbo-go-middleware-redis](https://gitee.com/changeden/dubbo-go-middleware-redis)       | memory cache middleware     | v0.1.5  |
| dubbo-go-middleware-web      | [gitee.com/changeden/dubbo-go-middleware-web](https://gitee.com/changeden/dubbo-go-middleware-web)           | web service middleware      | v0.1.5  |
| dubbo-go-middleware-dubbo    | [gitee.com/changeden/dubbo-go-middleware-dubbo](https://gitee.com/changeden/dubbo-go-middleware-dubbo)       | dubbo middleware            | v0.1.5  |
